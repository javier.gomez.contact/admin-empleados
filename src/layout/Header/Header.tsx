import React, { useState } from 'react'
import './Header.css'
import logo from '../../assets/images/logo.png'

const Header = () => {

    const [styleHeader, setStyleHeader] = useState({
        boxShadow: 'unset',
        backgroundColor: 'unset'
    })

    window.onscroll = function() {
        if(window.scrollY > 0)
            setStyleHeader({
                boxShadow: "0 3px 5px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%)",
                backgroundColor: "#2b296a"
            })
        else
            setStyleHeader({
                boxShadow: 'unset',
                backgroundColor: 'unset'
            })
    }

    return (
        <header id="content_header" style={styleHeader}>
            <div className="container_dda">
                <div className="row row_center">
                    <div className="logo-responsive">
                        <a id="logo" className="" title="Empleados | Cidenet" href="http://localhost:3000/" rel="home">
                            <img alt="Cidenet" className="img-responsive lazyloaded" src={logo} />
                        </a>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header
